#pragma once

#include "triangle.h"

class interpolated_triangle: public triangle
{
public:
	/*interpolated_triangle(triangle& triangle);*/
	point find();

private:

	point p0__;
	point p1__;
	point p2__;

	void count_color();
	void fill();
	
};

