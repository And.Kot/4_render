#pragma once

#include "canvas.h"
#include "point.h"
#include <vector>

class line: public point
{
public:
	line();
	line(point& p0, point& p1);

	void draw(canvas& buffer);

private:
	point p0_;
	point p1_;

	std::vector<point> p_vector;
	void fill();
};

