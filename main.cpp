#include <iostream>
#include "canvas.h"
#include <SDL.h>
#include "point.h"
#include "line.h"
#include "triangle.h"
#include "interpolated_triangle.h"

int main(int argc, char* args[])
{
	// task 01
	color green = { 0, 255, 0 };
	color blue = { 0, 0, 255 };
	color red = { 255, 0, 0 };
	color black = { 0, 0, 0 };
	color white = { 255, 255, 255 };
	color yellow = { 255, 255, 0 };

	canvas image;
	std::fill(image.begin(), image.end(), green);

	image.save_image("image.ppm");

	canvas image_loader;
	image_loader.load_image("image.ppm");
	image_loader.save_image("image2.ppm");

	// task 02
	point p1(10, 10, blue);
	point p2(300, 120, blue);
	image.clear(green);
	p1.draw(image);
	image.save_image("image_02.ppm");

	// task 03
	line l(p1, p2);
	image.clear(green);
	l.draw(image);
	image.save_image("image_03.ppm");

	// task 04
	point pt1(12, 67, blue), pt2(67, 12, blue), pt3(100, 200, blue);
	triangle t(pt1, pt2, pt3);
	image.clear(green);
	t.draw(image);
	image.save_image("image_04.ppm");

	// task 05
	interpolated_triangle i_t();

	i_t.find();

	return 0;
}