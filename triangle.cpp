#include "triangle.h"

triangle::triangle(point& p0, point& p1, point& p2):
	line(),
	l01_(p0, p1),
	l12_(p1, p2),
	l20_(p2, p0),
	p0_(p0),
	p1_(p1),
	p2_(p2)
{
}

void triangle::draw(canvas& buffer)
{
	l01_.draw(buffer);
	l12_.draw(buffer);
	l20_.draw(buffer);
}
