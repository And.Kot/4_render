#pragma once

#include "line.h"

class triangle: public line
{
public:
	triangle(point& p0, point& p1, point& p2);

	void draw(canvas& buffer);

protected:
	point p0_;
	point p1_;
	point p2_;

	line l01_;
	line l12_;
	line l20_;

	std::vector<point> p_vector;
};

