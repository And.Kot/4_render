#include "interpolated_triangle.h"

//interpolated_triangle::interpolated_triangle(triangle& triangle)
//{
//}

void interpolated_triangle::count_color()
{
	int x0 = p0_.get_x();
	int y0 = p0_.get_y();

	int x1 = p1_.get_x();
	int y1 = p1_.get_y();

	int x2 = p2_.get_x();
	int y2 = p2_.get_y();

	int r_color_0 = p0_.r;
	int g_color_0 = p0_.g;
	int b_color_0 = p0_.b;

	int r_color_1 = p1_.r;
	int g_color_1 = p1_.g;
	int b_color_1 = p1_.b;

	int r_color_2 = p2_.r;
	int g_color_2 = p2_.g;
	int b_color_2 = p2_.b;

	// R color
	int a_r = (y1 - y0) * (r_color_2 - r_color_0) - (y2 - y0) * (r_color_1 - r_color_0);
	int b_r = -(x2 - x0) * (r_color_1 - r_color_0) + (x1 - x0) * (r_color_2 - r_color_0);
	int c_r = (x2 - x0) * (y1 - y0) - (x1 - x0) * (y2 - y0);

	// G color
	int a_g = (y1 - y0) * (g_color_2 - g_color_0) - (y2 - y0) * (g_color_1 - g_color_0);
	int b_g = -(x2 - x0) * (g_color_1 - g_color_0) + (x1 - x0) * (g_color_2 - g_color_0);
	int c_g = (x2 - x0) * (y1 - y0) - (x1 - x0) * (y2 - y0);

	// B color
	int a_b = (y1 - y0) * (b_color_2 - b_color_0) - (y2 - y0) * (b_color_1 - b_color_0);
	int b_b = -(x2 - x0) * (b_color_1 - b_color_0) + (x1 - x0) * (b_color_2 - b_color_0);
	int c_b = (x2 - x0) * (y1 - y0) - (x1 - x0) * (y2 - y0);

	int x_ = p_vector.at(0).get_x();
	int y_ = p_vector.at(0).get_y();

	int result_r = r_color_0 - int((a_r * (x_ - x0) + b_r * (y_ - y0)) / (c_r));
	int result_g = g_color_0 - int((a_g * (x_ - x0) + b_g * (y_ - y0)) / (c_g));
	int result_b = b_color_0 - int((a_b * (x_ - x0) + b_b * (y_ - y0)) / (c_b));

	color c = { result_r, result_g, result_b };

	//return c;
}

void interpolated_triangle::fill()
{
}

point interpolated_triangle::find()
{
	

	int arr_y[] = { p0_.get_y(), p1_.get_y(), p2_.get_y() };
	int arr_x[] = { p0_.get_x(), p1_.get_x(), p2_.get_x() };

	int temp_y;
	int temp_x;

	for (int i = 0; i < 3; i++) {
		for (int j = i; j < 3 - 1; j++) {
			if (arr_y[i] > arr_y[j + 1])
			{
				temp_y = arr_y[i];
				temp_x = arr_x[i];

				arr_y[i] = arr_y[j + 1];
				arr_x[i] = arr_x[j + 1];

				arr_y[j + 1] = temp_y;
				arr_x[j + 1] = temp_x;
			}
		}
	}

	point p0(arr_x[0], arr_y[0], {0,0,0});

	size_t(pos) = std::find(p_vector.begin(), p_vector.end(), p0) - p_vector.begin();

	return p_vector.at(pos);
}
