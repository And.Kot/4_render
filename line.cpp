#include "line.h"

line::line():
	point()
{
}

line::line(point& p0, point& p1):
	point(),
	p0_(p0),
	p1_(p1)
{
	fill();
}

void line::draw(canvas& buffer)
{
	for (size_t i = 0; i < p_vector.size(); i++)
	{
		*(buffer.begin() + p_vector.at(i).get_x() + p_vector.at(i).get_y() * image_width) = *this;
	}
}

void line::fill()
{
	size_t x0 = p0_.get_x();
	size_t y0 = p0_.get_y();

	size_t x1 = p1_.get_x();
	size_t y1 = p1_.get_y();

	size_t delta_x = std::max(x0, x1) - std::min(x0, x1);
	size_t delta_y = std::max(y0, y1) - std::min(y0, y1);

	size_t delta_x_temp;
	size_t delta_y_temp;

	size_t dx;
	size_t dy;

	p_vector.push_back(point(x0, y0, { 0,0,0 }));

	if (x1 >= x0 && y1 >= y0)
	{
		if (delta_x == delta_y)
		{
			for (size_t i = 0; i < delta_x; i++)
			{
				x0++;
				y0++;
				p_vector.push_back(point(x0, y0, { 0,0,0 }));
			}
		}
		else if (delta_x > delta_y)
		{
			for (std::size_t i = 0; i < delta_x; i++)
			{
				delta_x_temp = std::max(p_vector.back().get_x(), x1) - std::min(p_vector.back().get_x(), x1);
				delta_y_temp = std::max(p_vector.back().get_y(), y1) - std::min(p_vector.back().get_y(), y1);

				dx = std::max(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * delta_x_temp * delta_y_temp)
					- std::min(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * delta_x_temp * delta_y_temp);

				dy = std::max(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * (delta_x_temp - 1) * (delta_y_temp - 1))
					- std::min(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * (delta_x_temp - 1) * (delta_y_temp - 1));

				if (dx >= dy)
				{
					y0++;
				}
				x0++;
				p_vector.push_back(point(x0, y0, { 0,0,0 }));
			}
		}
		else
		{
			for (std::size_t i = 0; i < delta_y; i++)
			{
				delta_x_temp = std::max(p_vector.back().get_x(), x1) - std::min(p_vector.back().get_x(), x1);
				delta_y_temp = std::max(p_vector.back().get_y(), y1) - std::min(p_vector.back().get_y(), y1);

				dx = std::max(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * delta_x_temp * delta_y_temp)
					- std::min(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * delta_x_temp * delta_y_temp);

				dy = std::max(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * (delta_x_temp - 1) * (delta_y_temp - 1))
					- std::min(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * (delta_x_temp - 1) * (delta_y_temp - 1));

				if (dx < dy)
				{
					x0++;
				}
				y0++;
				p_vector.push_back(point(x0, y0, { 0,0,0 }));
			}
		}
	}
	else if (x1 < x0 && y1 >= y0)
	{
		if (delta_x == delta_y)
		{
			for (std::size_t i = 0; i < delta_x; i++)
			{
				x0--;
				y0++;
				p_vector.push_back(point(x0, y0, { 0,0,0 }));
			}
		}
		else if (delta_x > delta_y)
		{
			for (size_t i = 0; i < delta_x; i++)
			{
				delta_x_temp = std::max(p_vector.back().get_x(), x1) - std::min(p_vector.back().get_x(), x1);
				delta_y_temp = std::max(p_vector.back().get_y(), y1) - std::min(p_vector.back().get_y(), y1);

				dx = std::max(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * delta_x_temp * delta_y_temp)
					- std::min(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * delta_x_temp * delta_y_temp);

				dy = std::max(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * (delta_x_temp - 1) * (delta_y_temp - 1))
					- std::min(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * (delta_x_temp - 1) * (delta_y_temp - 1));

				if (dx >= dy)
				{
					y0++;
				}
				x0--;
				p_vector.push_back(point(x0, y0, { 0,0,0 }));
			}
		}
		else
		{
			for (std::size_t i = 0; i < delta_y; i++)
			{
				delta_x_temp = std::max(p_vector.back().get_x(), x1) - std::min(p_vector.back().get_x(), x1);
				delta_y_temp = std::max(p_vector.back().get_y(), y1) - std::min(p_vector.back().get_y(), y1);

				dx = std::max(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * delta_x_temp * delta_y_temp)
					- std::min(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * delta_x_temp * delta_y_temp);

				dy = std::max(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * (delta_x_temp - 1) * (delta_y_temp - 1))
					- std::min(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * (delta_x_temp - 1) * (delta_y_temp - 1));

				if (dx < dy)
				{
					x0--;
				}
				y0++;
				p_vector.push_back(point(x0, y0, { 0,0,0 }));
			}
		}
	}
	else if (x1 >= x0 && y1 < y0)
	{
		if (delta_x == delta_y)
		{
			for (std::size_t i = 0; i < delta_x; i++)
			{
				x0++;
				y0--;
				p_vector.push_back(point(x0, y0, { 0,0,0 }));
			}
		}
		else if (delta_x > delta_y)
		{
			for (std::size_t i = 0; i < delta_x; i++)
			{
				delta_x_temp = std::max(p_vector.back().get_x(), x1) - std::min(p_vector.back().get_x(), x1);
				delta_y_temp = std::max(p_vector.back().get_y(), y1) - std::min(p_vector.back().get_y(), y1);

				dx = std::max(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * delta_x_temp * delta_y_temp)
					- std::min(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * delta_x_temp * delta_y_temp);

				dy = std::max(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * (delta_x_temp - 1) * (delta_y_temp - 1))
					- std::min(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * (delta_x_temp - 1) * (delta_y_temp - 1));

				if (dx >= dy)
				{
					y0--;
				}
				x0++;
				p_vector.push_back(point(x0, y0, { 0,0,0 }));
			}
		}
		else
		{
			for (std::size_t i = 0; i < delta_y; i++)
			{
				delta_x_temp = std::max(p_vector.back().get_x(), x1) - std::min(p_vector.back().get_x(), x1);
				delta_y_temp = std::max(p_vector.back().get_y(), y1) - std::min(p_vector.back().get_y(), y1);

				dx = std::max(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * delta_x_temp * delta_y_temp)
					- std::min(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * delta_x_temp * delta_y_temp);

				dy = std::max(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * (delta_x_temp - 1) * (delta_y_temp - 1))
					- std::min(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * (delta_x_temp - 1) * (delta_y_temp - 1));

				if (dx < dy)
				{
					x0++;
				}
				y0--;
				p_vector.push_back(point(x0, y0, { 0,0,0 }));
			}
		}
	}
	else if (x1 < x0 && y1 < y0)
	{
		if (delta_x == delta_y)
		{
			for (std::size_t i = 0; i < delta_x; i++)
			{
				x0--;
				y0--;
				p_vector.push_back(point(x0, y0, { 0,0,0 }));
			}
		}
		else if (delta_x > delta_y)
		{
			for (std::size_t i = 0; i < delta_x; i++)
			{
				delta_x_temp = std::max(p_vector.back().get_x(), x1) - std::min(p_vector.back().get_x(), x1);
				delta_y_temp = std::max(p_vector.back().get_y(), y1) - std::min(p_vector.back().get_y(), y1);

				size_t dx = std::max(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * delta_x_temp * delta_y_temp)
					- std::min(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * delta_x_temp * delta_y_temp);

				size_t dy = std::max(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * (delta_x_temp - 1) * (delta_y_temp - 1))
					- std::min(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * (delta_x_temp - 1) * (delta_y_temp - 1));

				if (dx >= dy)
				{
				}
				x0--;
				p_vector.push_back(point(x0, y0, { 0,0,0 }));
			}
		}
		else
		{
			for (std::size_t i = 0; i < delta_y; i++)
			{

				delta_x_temp = std::max(p_vector.back().get_x(), x1) - std::min(p_vector.back().get_x(), x1);
				delta_y_temp = std::max(p_vector.back().get_y(), y1) - std::min(p_vector.back().get_y(), y1);

				dx = std::max(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * delta_x_temp * delta_y_temp)
					- std::min(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * delta_x_temp * delta_y_temp);

				dy = std::max(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * (delta_x_temp - 1) * (delta_y_temp - 1))
					- std::min(delta_y * (delta_x_temp - 1) * delta_x_temp, delta_x * (delta_x_temp - 1) * (delta_y_temp - 1));

				if (dx < dy)
				{
					x0--;
				}
				y0--;
				p_vector.push_back(point(x0, y0, { 0,0,0 }));
			}
		}
	}
}
