#include "point.h"

point::point()
{
}

point::point(size_t x, size_t y, color c) :
	x_(x),
	y_(y),
	color(c)
{
}

void point::draw(canvas& buffer)
{
	*(buffer.begin() + x_ + y_ * image_width) = *this;
}

size_t point::get_x()
{
	return x_;
}

size_t point::get_y()
{
	return y_;
}

bool operator==(const point& left, const point& right)
{
	return (left.x_ == right.x_) && (left.y_ == right.y_);
}
